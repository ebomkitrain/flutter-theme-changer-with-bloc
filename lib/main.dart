import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:hydrated_bloc/hydrated_bloc.dart';
import 'package:theme_changer_with_bloc/theme.dart';
import 'bloc/theme_change_bloc.dart';
import 'bloc/theme_change_state.dart';
import 'home_page/home_page.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  BlocSupervisor.delegate = await HydratedBlocDelegate.build();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocProvider<ThemeChangeBloc>(
      create: (_) => ThemeChangeBloc(),
      child: BlocBuilder<ThemeChangeBloc, ThemeChangeState>(
          builder: (context, state) {
        return MaterialApp(
          title: 'Flutter Demo',
          themeMode: state.themeState.themeMode,
          darkTheme: darkTheme,
          theme: lightTheme,
          home: DartTutorialHomePage(),
        );
      }),
    );
  }
}
