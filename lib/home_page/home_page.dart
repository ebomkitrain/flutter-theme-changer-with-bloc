import 'package:flutter/material.dart';

import 'my_app_bar.dart';

class DartTutorialHomePage extends StatefulWidget {
  DartTutorialHomePage();
  @override
  _DartTutorialHomePageState createState() => _DartTutorialHomePageState();
}

class _DartTutorialHomePageState extends State<DartTutorialHomePage> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Scaffold(
        appBar: myAppBar(),
        body: ListView.separated(
          itemCount: contents.length,
          itemBuilder: (BuildContext context, int index) {
            return Card(
              child: ListTile(
                title: Text("${contents[index]}"),
              ),
            );
          },
          separatorBuilder: (BuildContext context, int index) {
            return Divider();
          },
        ),
      ),
    );
  }
}

final contents = [
  "Dart Overview",
  "Dart Syntax and keywords",
  "Dart Variable and Data types",
  "Dart Operators",
  "Dart Constant and Data types",
  "Dart Control flow Statement",
  "Dart Functions",
  "Dart Classes and Inheritance",
  "Dart Abstract Class and Interface",
  "Dart Libraries and Packages",
  "Dart Concurrency, Async and Future",
  "Asynchronous Programming Stream",
  "Dart Exception, Typedef and Debugging",
  "Dart Unit Testing"
];
